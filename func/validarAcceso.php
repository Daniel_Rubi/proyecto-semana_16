<?php
session_start();

if (isset($_POST['submit'])) {

    include "../class/Usuario.class.php";
    $usuario  = new Usuario();

    $usuario->nombre = strip_tags($_POST['txtUser']);
    $usuario->contrasenna = md5(strip_tags($_POST['txtPassword']));
    $res = $usuario->search();
    if (count($res) == 1) {
        $_SESSION['id'] = $res[0]['codUsuario'];
        $_SESSION['usuario'] = $res[0]['nombreUsuario'];
        $_SESSION['contraseña'] = $res[0]['contrasenna'];
        if (isset($_COOKIE['visitas'])) {
            setcookie('visitas',$_COOKIE['visitas'] + 1, time() + 3600 * 24,  '/', NULL, 0);
        } else {
            setcookie('visitas', 1, time() + 3600 * 24,  '/', NULL, 0);
        }
        header("Location:../");
    } else {
        if (isset($_COOKIE['numIntentos'])) {
            setcookie('numIntentos', $_COOKIE['numIntentos'] - 1, time() + 3600 * 24,  '/', NULL, 0);
        } else {
            setcookie('numIntentos', 3, time() + 3600 * 24,  '/', NULL, 0);
        }
        echo "<script>alert('El Usuario o la contraseña no son correctas');</script>";
        echo "<script>window.top.location.replace('../iniciar-sesion.php'); </script>";
    }
} else {
    header("Location:../");
}
