<?php
function mostrarMensaje($tipo, $mensaje)
{
    echo "
            <div class=\"alert alert-$tipo text-center alert-dismissible fade show mx-auto col-md-4 z-index-3\" role=\"alert\" id=\"alert\">
                <strong>$mensaje</strong> 
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
                    <span aria-hidden=\"true\">&times;</span>
                </button>
            </div>";
    echo "<script>setTimeout(\"document.getElementById('alert').style.visibility='hidden'\",3000);</script>";
}
?>
