<?php
session_start();

if (isset($_POST['submit'])) {

    include "../class/Usuario.class.php";
    $usuario  = new Usuario();
    $nombre = strip_tags($_POST['txtUser']);
    $contrasenna = strip_tags($_POST['txtPassword']);
    $repContra = strip_tags($_POST['txtRepeatPassword']);

    $usuario->nombre = $nombre;
    $res=  $usuario->getByName();
    if(count($res) >= 1){
        header("Location: ../registro.php?r=exist");
    }
    else if($contrasenna != $repContra){
        header("Location: ../registro.php?r=pass");
    }
    else{
        $usuario->nombre = $nombre;
        $usuario->contrasenna = md5($contrasenna);
        $res = $usuario->save();
        header("Location: ../iniciar-sesion.php?r=success&user=" . $nombre);
    }


} else {
    header("Location:../");
}
