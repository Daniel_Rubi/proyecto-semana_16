<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:../../iniciar-sesion.php");
}
include "../../class/Usuario.class.php";
$usuario = new Usuario();

if (isset($_POST['submit'])) {
    $nombre = strip_tags($_POST['nombre']);
    $contraseña = strip_tags($_POST['contrasenna']);
    $nuevaContraseña = strip_tags($_POST['nuevaContrasenna']);
    $repContraseña = strip_tags($_POST['repContrasenna']);

    $usuario->nombre = $nombre;
    $res =  $usuario->getByName();
    if (trim($nombre) == '') {
        header("Location: frmEditar.php?r=error&id=" . $_SESSION['id']);
    } else if (count($res) >= 1 && $nombre != $_SESSION['usuario']) {
        header("Location: frmEditar.php?r=exist&id=" . $_SESSION['id']);
    } else if (trim($contraseña) != '') {

        $usuario->codigo = $_SESSION['id'];
        $res = $usuario->getByCod();

        if (md5($contraseña) == $res[0]['contrasenna']) {
            if (($nuevaContraseña != $repContraseña)) {
                header("Location: frmEditar.php?r=equal&id=" . $_SESSION['id']);
            } else if (strlen($nuevaContraseña) <= 7) {
                header("Location: frmEditar.php?r=length&id=" . $_SESSION['id']);
            } else {
                $usuario->nombre = $nombre;
                $usuario->contrasenna = md5($nuevaContraseña);
                $res = $usuario->update();
                $_SESSION['usuario'] = $nombre;
                $_SESSION['contraseña'] = md5($nuevaContraseña);
                header("Location: ../../index.php?r=success");
            }
        } else {
            header("Location: frmEditar.php?r=pass&id=" . $_SESSION['id']);
        }
    } else {
        $usuario->codigo = $_SESSION['id'];
        $usuario->nombre = $nombre;
        $usuario->contrasenna = $_SESSION['contraseña'];
        $usuario->update();
        var_dump($nombre);

        $_SESSION['usuario'] = $nombre;

        header("Location: ../../index.php?r=success");
    }
}
