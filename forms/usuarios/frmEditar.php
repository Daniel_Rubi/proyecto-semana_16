<?php
session_start();
if (!isset($_SESSION['id'])) {
    header("Location:../../iniciar-sesion.php");
}
if (isset($_GET['r'])) {
    include "../../func/mensaje.php";
    if ($_GET['r'] == 'error') {
        mostrarMensaje('danger', 'El nombre de usuario no puede estar vacio!');
    } else if ($_GET['r'] == 'equal') {
        mostrarMensaje('danger', 'La contraseña nueva no coinciden!');
    } else if ($_GET['r'] == 'length') {
        mostrarMensaje('danger', 'La nueva contraseña debe tener minimo 8 caracteres!');
    } else if ($_GET['r'] == 'pass') {
        mostrarMensaje('danger', 'La contraseña actual no coincide!');
    } else if ($_GET['r'] == 'exist') {
        mostrarMensaje('danger', 'El nombre de usuario ya esta en uso!');
    }
}

if (isset($_GET['id'])) {
    if ($_GET['id'] == $_SESSION['id']) {
?>
        <!DOCTYPE html>
        <html lang="es">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Editar Usuario</title>
            <!-- BOOTSTRAP 4.4.1 -->
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
            <link rel="stylesheet" href="../../css/editar.css">
        </head>

        <body>
            <h1 class="text-center text-primary mt-2">Editar Usuario</h1>
            <div class="container p-4 d-flex justify-content-center ">
                <form class="col-md-8 card p-3 " action="editar.php" method="POST">
                    <input type="hidden" name="codigo" value="<?= $_SESSION['id']; ?>">
                    <div class="form-group">
                        <label for="carrera">Nombre de usuario</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value="<?= $_SESSION['usuario']; ?>">
                    </div>
                    <label class="text-primary" id="show">Cambiar contraseña</label>
                    <div id="resContra">
                        <div class="form-group">
                            <label for="contrasenna">Contraseña actual</label>
                            <input type="password" class="form-control" id="contrasenna" name="contrasenna">
                        </div>
                        <div class="form-group">
                            <label for="nuevaContrasenna">Nueva contraseña</label>
                            <input type="password" class="form-control" id="nuevaContrasenna" name="nuevaContrasenna">
                        </div>
                        <div class="form-group">
                            <label for="repContrasenna">Repita la nueva contraseña</label>
                            <input type="password" class="form-control" id="repContrasenna" name="repContrasenna">
                        </div>
                    </div>
                    <div class="d-flex justify-content-center">
                        <input type="submit" class="btn btn-primary col-md-4 ml-2 p-2" name="submit" value="Guardar"></input>
                        <a href="../../" class="btn btn-secondary col-md-4 ml-2 p-2">Cancelar</a>
                    </div>
                </form>
            </div>
    <?php } else {
        header("Location:../../index.php");
    }
} ?>
    <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("#show").click(function() {
            $('#resContra').toggle("swing");
        });
    </script>
        </body>

        </html>