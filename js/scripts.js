window.addEventListener('load', () => {
    var form = document.querySelector("#form");

    form.addEventListener('submit', (e) => {

        //Captura de datos
        var user = document.querySelector("#txtUser").value;
        var password = document.querySelector("#txtPassword").value;

        //Contraseña
        if (password.trim() == "") {
            document.querySelector(".messagePassword").style.display = "block";
            document.querySelector("#txtPassword").style.border = "1px solid red";
            document.querySelector("#txtPassword").focus();
            e.preventDefault();
        }
        else if (password.trim().length >= 0 && password.trim().length <= 7) {
            document.querySelector(".messagePassword").style.display = "block";
            document.querySelector("#txtPassword").style.border = "1px solid red";
            document.querySelector(".messagePassword").innerHTML = "Contraseña muy corta ";
            document.querySelector("#txtPassword").focus();
            e.preventDefault();
        }
        else {
            document.querySelector("#txtPassword").style.border = "1px solid rgba(0,188,212,1)";
            document.querySelector(".messagePassword").innerHTML = "";
        }

        //Usuario
        if (user.trim() == "") {
            document.querySelector("#txtUser").style.border = "1px solid red";
            document.querySelector("#txtUser").focus();
            e.preventDefault();
        }
        else {
            document.querySelector("#txtUser").style.border = "1px solid rgba(0,188,212,1)";
        }
        if (window.location.href.indexOf("registro") > -1) {
            var repPassword = document.querySelector("#txtRepeatPassword").value;

            if (repPassword.trim() == "") {
                document.querySelector("#txtRepeatPassword").style.border = "1px solid red";
                e.preventDefault();
            }
            else {
                document.querySelector("#txtRepeatPassword").style.border = "1px solid rgba(0,188,212,1)";
            }
        }

    });

    // MODAL DE OLVIDAR CONTRASEÑA
    var modal = document.getElementById("modalContra");
    var btn = document.getElementById("modal");
    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function () {
        modal.style.display = "block";
    }
    span.onclick = function () {
        modal.style.display = "none";
    }
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
});

//NUMERO DE INTENTOS
window.onload = actualizarReloj;

var tiempoTotal = 10;

function actualizarReloj() {
    document.getElementById('contador').innerHTML = "<strong>" + tiempoTotal + " segundos </sctrong>";
    if (tiempoTotal == 0) {
        document.querySelector('.modal-intentos').style.display = 'none';
        document.cookie = "numIntentos=3; expires=3600 * 24; path=/";
    } else {
        tiempoTotal -= 1;
        setTimeout("actualizarReloj()", 1000);
    }
}

