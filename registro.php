<?php
session_start();
if (isset($_SESSION['usuario'])) {
    header("Location: index.php");
}
if (isset($_GET['r'])) {
    include "func/mensaje.php";
    if ($_GET['r'] == 'exist') {
        mostrarMensaje('danger', 'Nombre de usuario ya existe');
    }
    if ($_GET['r'] == 'pass') {
        mostrarMensaje('danger', 'Las contraseñas no coinciden!');
    }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- ESTILOS -->
    <link rel="stylesheet" href="css/login.css">
</head>

<body>
    <div class="box">
        <div class="content-box">
            <div class="photo-user">
                <img src="images/profile.jpg" alt="">
            </div>
            <div class="content-form">
                <form action="func/registrar.php" method="POST" id="form">
                    <p>Registrarme<i class="far fa-hand-point-down"></i></p>
                    <span class="error messageUser"></span>
                    <input type="text" name="txtUser" id="txtUser" placeholder="Usuario">

                    <span class="error messagePassword"></span>
                    <input type="password" name="txtPassword" id="txtPassword" placeholder="Contraseña">

                    <input type="password" name="txtRepeatPassword" id="txtRepeatPassword" placeholder="Repita la contraseña">

                    <button type="submit" name="submit"><i class="fas fa-sign-in-alt"></i></button>
                    <a href="iniciar-sesion.php" class="register">Iniciar sesión</a>
                </form>
            </div>
        </div>
    </div>
    <script src="js/scripts.js"></script>

</body>

</html>