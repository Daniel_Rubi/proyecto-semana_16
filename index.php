<?php
session_start();
if (!isset($_SESSION['usuario'])) {
    header("Location:iniciar-sesion.php");
}
if (isset($_GET['r'])) {
    include "func/mensaje.php";
    mostrarMensaje('success', 'Perfil actualizado correctamente!');
}
if (isset($_COOKIE['numIntentos']) && isset($_SESSION['usuario'])) {
    setcookie('numIntentos', 3, time() + 3600 * 24,  '/', NULL, 0);
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Semana 16 - sesiones</title>
    <!-- BOOTSTRAP 4.4.1 -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/main.css" onsubmit="javaScript:tema();" id="estilos">
</head>

<body>
    <div class="container">
        <?php
        if ($_COOKIE['visitas'] >= 2) {
            echo '<p>Numero de visitas: ' . $_COOKIE['visitas'] . '</p>';
        } else {
            echo  '<p>Bienvenido por primera vez a nuesta web</p>';
        }
        ?>
        <h1>Bienvenido: <strong> <?= $_SESSION['usuario']; ?> </strong></h1>
        <div class="link">
            <a href="func/cerrarSesion.php" class="btn btn-secondary">Cerrar Sesión</a>
            <a href="forms/usuarios/frmEditar.php?id=<?= $_SESSION['id']; ?>" class="btn btn-primary">Editar mi perfil</a>
        </div>
        <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/41cc374f-74ad-4943-a38c-03eb7d29e91f/daam1tk-463bbfce-f080-475a-a0b3-772f318d18e6.gif?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3sicGF0aCI6IlwvZlwvNDFjYzM3NGYtNzRhZC00OTQzLWEzOGMtMDNlYjdkMjllOTFmXC9kYWFtMXRrLTQ2M2JiZmNlLWYwODAtNDc1YS1hMGIzLTc3MmYzMThkMThlNi5naWYifV1dLCJhdWQiOlsidXJuOnNlcnZpY2U6ZmlsZS5kb3dubG9hZCJdfQ.Y0vI5aDWrKwTCIMgURGbqlGsPx38C9xsrGPk6FXfWso" alt="Patata bailando">
        <p>Decidimos usar sesiones para el manejo de los datos del usuario puesto que es más seguro que las cookies</p>
        <p>Las cookies las usamos para el contador de visitas y contador de intentos al iniciar sesión (no se nos ocurrio nada más en que se puede usar). </p>
	<ul>
	  <li>Gerson Usiel Quintanilla Sánchez</li>
	  <li>Marcos Daniel Rubí Hernádez</li>
	</ul>
    </div>
</body>

</html>