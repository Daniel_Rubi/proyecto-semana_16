<?php
include "Crud.class.php";
$crud = new Crud();

class Usuario
{
    public $codigo;
    public $nombre;
    public $contrasenna;

    public function getAll()
    {
        global $crud;
        $crud->sql = "SELECT * FROM usuarios";
        return $crud->selectRows();
    }

    public function save()
    {
        global $crud;
        $crud->sql = "INSERT INTO usuarios
            (
                nombreUsuario,
                contrasenna
            )
                VALUES
            (
                :nombre,
                :contrasenna
            )";
        $vals = array(
            ":nombre" => $this->nombre,
            ":contrasenna" => $this->contrasenna
        );
        $crud->insert($vals);
    }

    public function update()
    {
        global $crud;
        $crud->sql = "UPDATE usuarios SET
            nombreUsuario=:nombre,
            contrasenna=:contrasenna
            WHERE codUsuario=:codigo";

        $vals = array(
            ":codigo" => $this->codigo,
            ":nombre" => $this->nombre,
            ":contrasenna" => $this->contrasenna
        );
        $crud->updateByID($vals);
    }
    public function search()
    {
        global $crud;
        $crud->sql = "SELECT * FROM usuarios WHERE nombreUsuario=:nombre AND contrasenna=:contrasenna";
        $vals = array(
            ":nombre" => $this->nombre,
            ":contrasenna" => $this->contrasenna
        );
        return $crud->select($vals);
    }
    public function getByCod()
    {
        global $crud;
        $crud->sql = "SELECT * FROM usuarios WHERE codUsuario=:codigo";
        $id = array(
            ":codigo" => $this->codigo
        );
        return $crud->getByID($id);
    }
    public function getByName()
    {
        global $crud;
        $crud->sql = "SELECT nombreUsuario FROM usuarios WHERE nombreUsuario=:nombre";
        $vals = array(
            ":nombre" => $this->nombre
        );
        return $crud->getByName($vals);
    }

    public function delete()
    {
        global $crud;
        $crud->sql = "DELETE FROM usuarios WHERE codUsuario=:codigo";
        $vals = array(":codigo" => $this->codigo);
        $crud->deleteById($vals);
    }
}
